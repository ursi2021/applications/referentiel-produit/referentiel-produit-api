let express = require('express');
let router = express.Router();
let logger = require('../logs');

let app = express();

router.get("", async (req, res, next) => {
	try {
		const jobs = [ // List of Treatments
			{
				j: 0, // 0 is everyday
				h: 7, // 7h
				m: 0, // 0 min
				date: null, // null because j = 0 (Recurrent Treatment)
				name: "Every day at 7", // Treatment's name
				route: "/referentiel-produit/send-products", // POST will be done on this route
				params : [{
          // No params
					}],// params will be sent in the POST body
			},
		];
		res.status(200).json(jobs);
	} catch (error) {
		logger.error(error); res.sendStatus(404);
	}
});

module.exports = router;
