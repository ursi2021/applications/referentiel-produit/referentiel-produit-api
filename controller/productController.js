const axios = require('axios');
const app = require('../app');
const Product = require('../models/product')(app);
var logger = require('../logs');

const { getProductFromAssortmentType, updateAssortmentsType, getNewProductAssortment, updateProducts } = require('../services/productService');
const { updateProductPrice, get_new_price } = require('../services/priceManagement');


async function updateAndGetProducts(assortmentType) {

  // get and update purchase-price
  product_price = await getProductPrice()
  console.log(product_price)
  products = await get_new_price(product_price)
  console.log(products)
  await updateProductPrice(products)
  console.log(products)

  // get and update product assortment
  products = await getNewProductAssortment(assortmentType);
  await updateAssortmentsType(products);
  newProducts = await getProductFromAssortmentType(assortmentType);

  return newProducts;
}

module.exports.updateAndGetProducts = updateAndGetProducts;

async function getProductPrice() {
  let response = await axios.post(process.env.URL_GESTION_COMMERCIALE + "/products-purchase-price").then(res => {

    // Filter response according to assortment type
    let data = res.data.filter(function(prices) {
      return (prices);
    });
    return data;

  })
  .catch(err => {
    console.log(err);
    return err;
  })
  return response;

}
module.exports.getProductPrice = getProductPrice;


async function updateAllPrice() {

  // get and update purchase-price
  product_price = await getProductPrice()

  if (product_price != null) {

    products = await get_new_price(product_price)
    
    if (products != null) {
      await updateProductPrice(products)
      return true
    }
  }
  
  return false

}

module.exports.updateAllPrice = updateAllPrice;


async function sendProducts() {

  const products = await Product.findAll();
  let response = await axios.post(process.env.URL_GESTION_COMMERCIALE + "/get-products", products)
  .then((response) => {
    logger.info(response);
    return '';
  }, (error) => {
    console.log(error);
  });
  return '';
}

module.exports.sendProducts = sendProducts


async function updateProductsController(newProducts) {
  await updateProducts(newProducts);
  return '';
}

module.exports.updateProductsController = updateProductsController
