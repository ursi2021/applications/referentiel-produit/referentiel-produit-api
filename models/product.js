const logger = require('../logs')
const { Sequelize, DataTypes } = require('sequelize');

const fs = require('fs');


// login to mariadb database
// ex : new  new Sequelize(<dbname>, <user>, <password>, ...);
const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER,process.env.URSI_DB_PASSWORD, {
  host: process.env.URSI_DB_HOST,
  port: Number(process.env.URSI_DB_PORT),
  logging: msg => logger.verbose(msg),
  dialectOptions: {
    timezone: 'Etc/GMT0'
  },
  dialect: 'mariadb'
});

try {
  sequelize.authenticate();
  logger.info('Connection has been established successfully.');
} catch (error) {
  logger.error('Unable to connect to the database:', error);
}

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          firstName:
 *              type: string
 *          lastName:
 *              type: string
 */
const Product = sequelize.define('Product', {
  // Model attributes are defined here
  'product-code': {
    type: DataTypes.STRING,
    allowNull: false
  },
  'product-family': {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  'product-description': {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  'min-quantity': {
    type: DataTypes.INTEGER
    // allowNull defaults to true
  },
  'packaging': {
    type: DataTypes.INTEGER
    // allowNull defaults to true
  },
  'price': {
    type: DataTypes.FLOAT
    // allowNull defaults to true
  },
  'assortment-type': {
    type: DataTypes.STRING
    // allowNull defaults to true
  }

}, {
  timestamps: false
  // Other model options go here
});


Product.sync();
logger.info("The table for the Product model was just (re)created!");

// Add product from json
Product.findAll().then(function(prod) {
  //on récupère ici un tableau "users" contenant une liste d'utilisateurs
  if (prod.length == 0) {

    const rawdata = fs.readFileSync('./models/productModel.json');
    const insert = JSON.parse(rawdata)

    const cody = Product.bulkCreate(insert).then (res => {
    }).catch(function (e) {
      console.log(e);
    });
  }
}).catch(function (e) {
  //gestion erreur
  console.log(e);
});


var _app = {};
module.exports = function(app){
  _app = app;
  return Product
};
