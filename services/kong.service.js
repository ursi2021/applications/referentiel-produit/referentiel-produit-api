const axios = require('axios');
const logger = require('../logs');

var registered = false;

async function register_kong() {
    const registerApp = await axios
        .post('http://kong:8081/services/', {
            name: process.env.APP_NAME,
            url: 'http://' + process.env.APP_NAME
        })
        .then(res => {
            logger.info(`Service id : ${res.data.id}`)
            logger.info(`Succesfully register service to kong; statusCode: ${res.statusCode}`)
            return true;
        })
        .catch(error => {
            logger.error('Kong Service register : Axios Error.\n'+ error)
            return false;
        })


    // if (!registerApp) return false;
    const registerRoute = await axios
        .post('http://kong:8081/services/' + process.env.APP_NAME + '/routes', {
            paths: [
                    "/" + process.env.APP_NAME,
                    "/api/changeProductPrice",
                    "/" + process.env.APP_NAME + "/hello",
                    "/" + process.env.APP_NAME + "/hello/all",
                    "/users",
                    "/clients",
                    "/products",
                    "/api/user",
                    "/" + process.env.APP_NAME + "/clients",
                    "/" + process.env.APP_NAME + "/products",
                    "/" + process.env.APP_NAME + "/list_products",
                    "/" + process.env.APP_NAME + "/products/store",
                    "/" + process.env.APP_NAME + "/products/web",
                    "/" + process.env.APP_NAME + "/products/products-purchase-price",
                    "/" + process.env.APP_NAME + "/products/prices",
                    "/clock-register",
                    "/api/user",
                    "/api/client",
                    "/api/product",
                    "/" + process.env.APP_NAME + "/send-products",
                    "/" + process.env.APP_NAME + "/update-products"
                    ],
            name: process.env.APP_NAME
        })
        .then(res => {
            logger.info(`Route id : ${res.data.id}`)
            logger.info(`Succesfully register route to kong; tatusCode: ${res.statusCode}`)
            return true;
        })
        .catch(error => {
            logger.error('Kong Route register : Axios Error.\n' + error)
            return false;
        })
    return registerApp && registerRoute;
}

async function register_process() {
    var test = 0
    while (registered === false && test < 5) {
        test++;
        registered = await register_kong();
    }

    if (test > 4) {
        logger.error('Error Kong, cannot connect')
    }
}

module.exports = register_process;
