const app = require('../app')
const axios = require('axios')
var Product = require('../models/product')(app)
const fs = require('fs');
const logger = require('../logs');

const { get_new_price } = require('../services/priceManagement');




module.exports.getProductFromAssortmentType = getProductFromAssortmentType
module.exports.getNewProductAssortment = getNewProductAssortment
module.exports.updateAssortmentsType = updateAssortmentsType
module.exports.updateProducts = updateProducts

function filterProductByAssortment(products, assortmentType) {
  return products.filter(function(product) {
    return (product['assortment-type'] == assortmentType || product['assortment-ype'] == 'Both')
      && product
  })
}


/*
 * getProductFromAssortmentType
 */
async function getProductFromAssortmentType(assortmentType) {
  return Product.findAll({
    where:{
      'assortment-type': [assortmentType, "Both"]
    }
  })
  .then((list) => {return list})
}


/*
 * getProductAssortment
 */
async function getNewProductAssortment(type) {
  let response = await axios.get(process.env.URL_GESTION_COMMERCIALE + '/products/' + type)
    .then(res => {
      // Filter response according to assortment type
      return filterProductByAssortment(res.data, type)
    })
  .catch(err => {
    console.log(err);
    return err;
  })
  return response;
}


/*
 * updateAssortmentsType
 */
async function updateAssortmentsType(products) {

  const rawdata = fs.readFileSync('./models/productModel.json')
  const jsonProducts= JSON.parse(rawdata)


  for (let product of products) {

    Product.findAll({
      where:{
        'product-code': product['product-code']
      }
    })
    .then((list) => {
      list.forEach(old_product => {
        old_product["assortment-type"] = product["assortment-type"];
        old_product.save();
      })
    });

    for (let old_product of jsonProducts) {
      if (old_product["product-code"] == product['product-code'])
      {
        old_product["assortment-type"] = product['assortment-type']
      }
    }
  }

  let data = JSON.stringify(jsonProducts, null, 2);
  await fs.writeFile("./models/productModel.json", data, (err) => {
    if (err) console.log("[ERROR]: " + str(err));
  });
}


/*
 * updateProducts
 */
async function updateProducts(products) {

  products = await get_new_price(products);

  // Get json products
  const rawdata = fs.readFileSync('./models/productModel.json')
  const jsonProducts= JSON.parse(rawdata)


  for (let product of products) {

    await Product.findAll({
      where:{
        'product-code': product['product-code']
      }
    })
    .then((list) => {
      list.forEach(old_product => {
        logger.info('code: ' +  product['product-code'] +  ", change assortment : " + old_product["assortment-type"] +  ' to ', product["assortment-type"] +  ' and new price: ' + product["price"]);

        old_product["assortment-type"] = product["assortment-type"];
        old_product["price"] = product["price"];
        old_product.save();
      })
    });

    for (let old_product of jsonProducts) {
      if (old_product["product-code"] == product['product-code'])
      {
        old_product["assortment-type"] = product['assortment-type'];
        old_product["price"] = product["price"];
      }
    }
  }

  let data = JSON.stringify(jsonProducts, null, 2);
  await fs.writeFile("./models/productModel.json", data, (err) => {
    if (err) console.log("[ERROR]: " + str(err));
  });

  logger.info('FIN PRODUCT SERVICE');
  return '';
}

