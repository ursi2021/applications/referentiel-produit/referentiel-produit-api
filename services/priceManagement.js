const app = require('../app')
const fs = require('fs');
const logger = require('../logs');

const Product = require('../models/product')(app)

const logistic_cost = require('../config/logistic_cost.json')
const taxes = require('../config/taxes.json')



async function updateProductPrice(products) {

  const rawdata = fs.readFileSync('./models/productModel.json');
  console.log(rawdata);
  const jsonProducts= JSON.parse(rawdata)

  for (let product of products) {

    await Product.findAll({
      where:{
        'product-code': product['product-code']
      }
    })
    .then((list) => {
      list.forEach(old_product => {
        old_product["price"] = product["price"];
        old_product.save();
      })
    });

    for (let old_product of jsonProducts) {
      if (old_product["product-code"] == product['product-code'])
      {
        old_product["price"] = product['price'];
      }
    }
  }

  let data = JSON.stringify(jsonProducts, null, 2);
  await fs.writeFile("./models/productModel.json", data, (err) => {
    if (err) console.log("[ERROR]: " + str(err));
  });

}

module.exports.updateProductPrice = updateProductPrice


async function get_new_price(product_price) {

  try {

    let list_products = []
    for (let product of product_price) {

        await Product.findAll({
          where:{
            'product-code': product['product-code']
          }
        })
        .then((list) => {
          list.forEach(old_product => {

            price = product['purchase-price'] 
            price += product['purchase-price'] * (logistic_cost[old_product['product-family']] / 100);
            price += product['purchase-price'] * (taxes[old_product['product-family']] / 100);
            
            list_products.push({'price': Number((price).toFixed(2)), 'product-family': old_product['product-family'],
                                'product-code': product['product-code'], "assortment-type": product["assortment-type"]})

          })
        });
           
    }
  
    return list_products;

  } catch (e) {
      console.log('[ERROR]: ' + e);
      return null;
  }

  // try {

  //   for (const product of product_price) {
  //       price = product['purchase-price'];
  //       price += product['purchase-price'] * (logistic_cost[product['product-family']] / 100);
  //       price += product['purchase-price'] * (taxes[product['product-family']] / 100);
  //       product['price'] = Number((price).toFixed(2));        
  //   }
  //   return product_price;

  // } catch (e) {
  //     console.log('[ERROR]: ' + str(e));
  //     return null;
  // }


}

module.exports.get_new_price = get_new_price