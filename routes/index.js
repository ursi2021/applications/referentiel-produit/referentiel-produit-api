express = require('express');
var router = express.Router();
var app = express();
var logger = require('../logs');

var User = require('../models/user_template')(app);
var Client = require('../models/client_template')(app);
var Product = require('../models/product')(app);

var template = require('../controller/template_controller')(app);
const request =require('sync-request');

// const ClientControler = require('../controller/clients');
const axios = require("axios");

const productController = require('../controller/productController');

const productService =  require('../services/productService')



/**@swagger
 * /api/changeProductPrice:
 *   post:
 *     tags:
 *       - Products
 *     summary: change product price
 *     responses:
 *       200:
 *         description: product price has been modified
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       422:
 *         description: Failed to find product and/or change price
 */
router.post('/api/changeProductPrice', function(req, res, next){
   t = Product.findAll({
    where:{
      'product-code': req.body.productCode
    }
  }).then(
    (res) => {
      if (t){
        Product.update({ price: req.body.productPrice }, {
          where: {
            'product-code': req.body.productCode
          }
        });
        res.status(200).json({'result': 'success'})
      }else{
        res.status(422).json({'result': 'could not find product with given ID'})
      }
    }
  )
});

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/


router.get('/', template.index)


/**@swagger
 * /referentiel-product/hello:
 *   get:
 *     tags:
 *       - Products
 *     summary: send hello world to other
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an string hello world
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/hello', function(req, res, next) {
    try {
        res.status(200).json({'referentiel-produit': 'Hello World !'})
    } catch (e) {
        res.status(404).json({'error': 'FindAll'});
    }
});


/**@swagger
 * /referentiel-produit/hello/all:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all hello from all application
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return result of get hello from all application
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/hello/all', function (req, res, next) {

    let otherApps = [process.env.URL_CAISSE + '/hello', process.env.URL_DECISIONNEL + '/hello', process.env.URL_E_COMMERCE + "/hello", process.env.URL_GESTION_COMMERCIALE + '/hello',
        process.env.URL_ENTREPROTS + '/hello', process.env.URL_RELATION_CLIENT + '/hello', process.env.URL_GESTION_PROMOTION + '/hello', process.env.URL_MONETIQUE_PAIEMENT + '/hello',
        process.env.URL_BACK_OFFICE_MAGASIN + '/hello']

    let responses = [];

    for (const appUriIndex in otherApps) {
        var appURI = otherApps[appUriIndex];
        console.log('trying to say hello to ' + appURI);
        let resObj = {"URI": "none", "message": "none"};
        resObj["URI"] = appURI;

        const resRequest = request('GET', appURI);
        if (resRequest.isError()) {
            console.error(`Could not send request: ${resRequest.body}`);
            resObj["message"] = `Could not send request: ${resRequest.body}`;
        } else if (resRequest.statusCode !== 200) {
            console.error(`Expected status code 200 but received ${resRequest.statusCode}.`);
            resObj["message"] = `Expected status code 200 but received ${resRequest.statusCode}.`;
        } else {
            console.log('SUCCESS');
            resObj["message"] = 'SUCCESS: ' + resRequest.body;
        }

        responses.push(resObj);

    }
    res.render('hellos', {title: 'Hello des Apps', answers: responses});

        /* test FIXME eradicate me
        request('https://google.com/', (error, response, body) => {
            if (error) {
                console.error(`Could not send request: ${error.message}`);
                return;
            }

            if (response.statusCode != 200) {
                console.error(`Expected status code 200 but received ${response.statusCode}.`);
                return;
            }

            console.log('Got It !');
            res.render('hellos', {title: 'Hello des Apps'});
        });*/
    }
)


/**@swagger
 * /users:
 *   get:
 *     tags:
 *       - Products
 *     summary: get users page
 *     responses:
 *       200:
 *         description: Return users html
 */
router.get('/users',
    function(req, res, next)
    {
        User.findAll().then(function(users)
        {
            logger.info(users);
            res.render('users', {title: 'Users', users: users});
        });
    });


/**@swagger
 * /clients:
 *   get:
 *     tags:
 *       - Products
 *     summary: add clients page
 *     responses:
 *       200:
 *         description: Return clients html
 */
router.get('/clients', function(req, res, next) {

    var col_name = Object.keys(Client.rawAttributes)

    // Del id
    const index = col_name.indexOf('id');
    if (index > -1) {
        col_name.splice(index, 1);
    }

  //
  Client.findAll().then(function(clients) {
    logger.info(clients);
    res.render('clients', {title: 'Clients', clients: clients, colum_name: col_name});
  });
});


/**@swagger
 * /products:
 *   get:
 *     tags:
 *       - Products
 *     summary: add products page
 *     responses:
 *       200:
 *         description: Return products html
 */
router.get('/products', function(req, res, next) {

    var col_name = Object.keys(Product.rawAttributes)

    // Del id
    const index = col_name.indexOf('id');
    if (index > -1) {
        col_name.splice(index, 1);
    }

  Product.findAll().then(function(products) {
    logger.info(products);
    res.render('products', {title: 'Produits', products: products, colum_name: col_name});
  });
});


/**@swagger
 * /api/user:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all users
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of users
 *         schema:
 *           type: array
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/api/user', function(req, res, next) {
  try {
    User.findAll().then(user => res.status(200).json(user));
  } catch (e) {
    res.status(404).json({'error': 'FindAll'});
  }
});


/**@swagger
 * /referentiel-produit/clients:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all client
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of client
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Client'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/clients', async function(req, res, next) {

    var result = '';
    await axios.get(process.env.URL_RELATION_CLIENT + '/clients').then(function (clients) {
        result = clients.data;
    }).catch((err) => {
        result = err.data == undefined ? {"myerror": "My error : return data type /clients is undefined: maybe table is empty"} : err.data;
    });
    res.status(404).json(result);

});


router.post('/referentiel-produit/send-products', async function(req, res, next) {
  try {
    await productController.sendProducts().then(response => {
      res.status(200).json({'OK': 'OK'});
    });

  } catch (e) {
    res.status(404).json({'error': 'send products, ' + e});
  }

});


router.post('/referentiel-produit/update-products', async function(req, res, next) {
  try {
    await productController.updateProductsController((req.body));
  } catch (e) {
    res.status(404).json({'error': 'update-products, ' + e});
  }

  res.status(200).json({'OK': 'OK'});

});


/**@swagger
 * /referentiel-produit/products:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all product
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of product
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/products', async function(req, res, next) {
  try {
    await productController.updateAllPrice().then(response => {
      Product.findAll().then(product => res.status(200).json(product));
    });

  } catch (e) {
    res.status(404).json({'error': 'FindAll, ' + e});
  }

});


/**@swagger
 * /referentiel-produit/list_products:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all product
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of product
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/list_products', function(req, res, next) {

  try {

    Product.findAll().then(product => res.status(200).json(product));

  } catch (e) {
    res.status(404).json({'error': 'FindAll, ' + e});
  }
});


/**@swagger
 * /referentiel-produit/products/store:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all product where assortmentType = store
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of product
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/products/store', async function(req, res, next) {
  try {
    await productController.updateAndGetProducts('Store')
    .then(assortments => {

      res.status(200).json(assortments)
    });
  } catch (e) {
    res.status(404).json({'error': 'Get store assortments, error: ' + e});
  }
});


/**@swagger
 * /referentiel-produit/products/web:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all product where assortmentType = web
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of product
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/products/web', function(req, res, next) {
  try {
    productController.updateAndGetProducts('Web')
    .then(assortments => {

      res.status(200).json(assortments)
    });
  } catch (e) {
    res.status(404).json({'error': 'Get web assortments'});
  }
});


/**@swagger
 * /referentiel-produit/products/both:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all product where assortmentType = both
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of product
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Product'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/referentiel-produit/products/both', function(req, res, next) {
  try {
    productController.updateAndGetProducts('Both')
    .then(assortments => {

      res.status(200).json(assortments)
    });
  } catch (e) {
    res.status(404).json({'error': 'Get Both assortments'});
  }
});

router.get('/referentiel-produit/products-purchase-price', function(req, res, next) {
  try {
    productController.getProductPrice().then(prices => {
      res.status(200).json(prices)
    });
  } catch (e) {
    res.status(404).json({'error': 'Get purchase prices'});
  }
});

router.get('/referentiel-produit/prices', function(req, res, next) {
    res.render('prices');
});


/**@swagger
 * /api/user:
 *   post:
 *     summary: get all users
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: Return created user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to create data
 */
router.post('/api/user', function(req, res, next) {
    logger.info(req.body);
    User.create(req.body).then(function (user) {

      res.status(200).json(user);
    });

});

router.post('/api/client', function(req, res, next) {
    logger.info(req.body);
    Client.create(req.body).then(function (client) {

      res.status(200).json(client);
    });

});

router.post('/api/product', function(req, res, next) {
    logger.info(req.body);
    Product.create(req.body).then(function (product) {

      res.status(200).json(product);
    });

});

module.exports = router;

// Clock - Send products
router.post('/referentiel-produit/clock-products', async function(req, res, next) {
    try {
      await productController.sendProducts().then(response => {
        res.status(200).json({'OK': 'OK'});
      });

    } catch (e) {
      res.status(404).json({'error': 'send products, ' + e});
    }
  }
);
